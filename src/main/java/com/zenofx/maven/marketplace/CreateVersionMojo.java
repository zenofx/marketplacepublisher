/**
 * Atlassian Marketplace Publisher
 * Copyright (C) 2019 zenofx.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */
package com.zenofx.maven.marketplace;

import java.io.File;
import java.util.StringTokenizer;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Proxy;
import org.joda.time.LocalDate;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MarketplaceClientFactory;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonQuery;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.http.HttpConfiguration;
import com.atlassian.marketplace.client.http.HttpConfiguration.Credentials;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthMethod;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyAuthParams;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyConfiguration;
import com.atlassian.marketplace.client.http.HttpConfiguration.ProxyHost;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionStatus;
import com.atlassian.marketplace.client.model.HtmlString;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.ModelBuilders.AddonVersionBuilder;
import com.atlassian.marketplace.client.model.ModelBuilders.InvalidModelException;
import com.atlassian.marketplace.client.model.PaymentModel;

/**
 * Create new app version in the Atlassian Marketplace.
 * 
 * @author tom
 */
@Mojo( name = "createVersion", defaultPhase = LifecyclePhase.DEPLOY )
public class CreateVersionMojo extends AbstractMojo {
    /**
     * Default build pattern as proposed by Atlassian.
     * Package private for testing.
     */
    static final String BUILD_PATTERN_DEFAULT = "%1d%03d%03d";

    /**
     * Username for Atlassian Marketplace.
     */
    @Parameter(property = "username", required = true)
    private String username;

    /**
     * Token for Atlassian Marketplace.
     */
    @Parameter(property = "password", required = true)
    private String password;

    /**
     * Status of version after upload (PRIVATE, PUBLIC or SUBMITTED).
     * This is only for completeness, please always use PRIVATE and set public manually.
     */
    @Parameter(defaultValue = "PRIVATE")
    private AddonVersionStatus status;

    /**
     * Release notes.
     */
    @Parameter(property = "releaseNotes")
    private String releaseNotes;

    /**
     * Release summary.
     */
    @Parameter(property = "releaseSummary", required = true)
    private String releaseSummary;

    /**
     * Type of artifact (JAR or OBR).
     */
    @Parameter(property = "type", defaultValue = "JAR", required = true)
    private ArtifactType type;

    /**
     * License type (COMMERCIAL, COMMERCIAL_FREE, BSD, ASL, GPL, LGPL, EPL or ATLASSIAN_CLOSED_SOURCE).
     */
    @Parameter(property = "licenseType", required = true)
    private LicenseType licenseType;

    /**
     * Payment Model (FREE, PAID_VIA_VENDOR or PAID_VIA_ATLASSIAN).
     */
    @Parameter(property = "paymentModel", required = true)
    private PaymentModel paymentModel;

    /**
     * Is this a beta version?
     */
    @Parameter(property = "beta", defaultValue = "false", required = true)
    private boolean beta;

    /**
     * Is this version supported?
     */
    @Parameter(property = "supported", defaultValue = "true", required = true)
    private boolean supported;

    /**
     * Pattern for computing build number from semantic version.
     * Default value according to Atlassian versioning proposals.
     */
    @Parameter(property = "buildPattern", defaultValue = BUILD_PATTERN_DEFAULT, required = true)
    private String buildPattern;

    /**
     * Build number, if not set computed from semantic version.
     */
    @Parameter(property = "buildNumber")
    private Long buildNumber;

    // Internal Parameters for accessing project and session.
    @Parameter(defaultValue = "${session}", readonly = true, required = true)
    private MavenSession session;

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    // Security dispatcher for decrypting password
    @Component( role = SecDispatcher.class, hint = "default" )
    private SecDispatcher securityDispatcher;

   /**
     * Execute our Mojo.
     * @see org.apache.maven.plugin.Mojo#execute()
     */
    public void execute() throws MojoExecutionException {
        // Get Marketplace credentials with (potentially) encrypted token
        Credentials credentials;
        try {
            credentials = new Credentials(username, securityDispatcher.decrypt( password));
        } catch (SecDispatcherException e) {
            throw new MojoExecutionException("Error decrypting password" , e);
        }

        // Open MarketplaceClient with credentials via proxy (if needed)
        HttpConfiguration httpConfig = HttpConfiguration.builder()
                .proxyConfiguration(getProxyConfiguration())
                .credentials(Option.option(credentials))
                .build();
        MarketplaceClient client = MarketplaceClientFactory.createMarketplaceClient(httpConfig);

        // Upload jar/obr for version
        String filename = project.getBuild().getFinalName() + "." + type.name().toLowerCase();
        File artifact = new File(project.getBuild().getDirectory(), filename);
        String app = project.getGroupId() + "." + project.getArtifactId();
        getLog().info("Uploading artifact " + artifact.getAbsolutePath() + " for " + app + " " + project.getVersion());
        
        ArtifactId artifactId;
        try {
            artifactId = client.assets().uploadAddonArtifact(artifact);
        } catch (MpacException e) {
            throw new MojoExecutionException("Error uploading artifact " + artifact.getAbsolutePath(), e);
        }

        // Get licenseType from list
        String licenseTypeKey = licenseType.name().toLowerCase().replace("_", "-");
        Option<com.atlassian.marketplace.client.model.LicenseType> licenseOption;
        try {
            licenseOption = client.licenseTypes().getByKey(licenseTypeKey);
        } catch (MpacException e) {
            throw new MojoExecutionException("Error getting license type" , e);
        }

        // Copy information from last version if any
        Option<AddonVersion> lastVersion;
        try {
            lastVersion = client.addons().getByKey(app, AddonQuery.builder().withVersion(true).build()).get().getVersion();
        } catch (MpacException e) {
            throw new MojoExecutionException("Error loading last version" , e);
        }
        AddonVersionBuilder versionBuilder;
        if (lastVersion.isDefined()) {
            versionBuilder = ModelBuilders.addonVersion(lastVersion.get());
        }
        else {
            versionBuilder = ModelBuilders.addonVersion();
        }

        // Build complete version object
        AddonVersion version;
        Option<HtmlString> releaseNotesOption = releaseNotes == null ? Option.none(HtmlString.class) : Option.option(HtmlString.html(releaseNotes)); 
        if (buildNumber == null) {
            buildNumber = parseBuildNumber(project.getVersion(), buildPattern);
        }
        try {
            version = versionBuilder.buildNumber(buildNumber).status(AddonVersionStatus.PRIVATE)
                    .name(project.getVersion()).artifact(Option.option(artifactId))
                    .releaseDate(new LocalDate()).beta(beta).supported(supported)
                    .paymentModel(paymentModel).licenseType(licenseOption)
                    .releaseSummary(Option.option(releaseSummary)).releaseNotes(releaseNotesOption).build();
        } catch (InvalidModelException e) {
            throw new MojoExecutionException("Error generating version" , e);
        }
        
        // Finally create the version with our version object and the uploaded artifact
        try {
            client.addons().createVersion(app, version);
        } catch (MpacException e) {
            throw new MojoExecutionException("Error creating version" , e);
        }
    }

    /**
     * Copy active maven proxy config to Marketplace API {@link ProxyConfiguration}.
     * 
     * @return {@link ProxyConfiguration}
     */
    private Option<ProxyConfiguration> getProxyConfiguration() {
        Proxy proxy = session.getSettings().getActiveProxy();
        if (proxy == null) {
            // No proxy
            return Option.none(ProxyConfiguration.class);
        }
        else {
            Option<ProxyAuthParams> authParams;
            if (proxy.getUsername() != null) {
                // Proxy with authentication
                Credentials credentials = new Credentials(proxy.getUsername(), proxy.getPassword());
                authParams = Option.option(new ProxyAuthParams(credentials, ProxyAuthMethod.BASIC));
            }
            else {
                // Proxy without authentication
                authParams = Option.none(ProxyAuthParams.class);
            }
            return Option.option(ProxyConfiguration.builder()
                    .proxyHost(Option.option(new ProxyHost(proxy.getHost(), proxy.getPort())))
                    .authParams(authParams)
                    .build());
        }
    }
    
    /**
     * Compute build number from semantic version by given pattern.
     * Package private for testing.
     * 
     * @param version Semantic version as string
     * @param pattern Build pattern
     * @return Build number as long
     * @throws NumberFormatException
     */
    long parseBuildNumber(String version, String pattern) throws NumberFormatException {
        StringTokenizer tok = new StringTokenizer(version, ".-");
        int x=0,y=0,z=0;
        if (tok.hasMoreTokens()) {
            x = Integer.parseInt(tok.nextToken());
            if (tok.hasMoreTokens()) {
                y = Integer.parseInt(tok.nextToken());
                if (tok.hasMoreTokens()) {
                    z = Integer.parseInt(tok.nextToken());
                }
            }
        }
        return Long.parseLong(String.format(pattern, x, y, z));
    } 
}
