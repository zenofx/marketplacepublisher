/**
 * Atlassian Marketplace Publisher
 * Copyright (C) 2019 zenofx.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/
 */
package com.zenofx.maven.marketplace;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CreateVersionMojoTest {
    
    @Test
    public void testDefaultPattern() {
        CreateVersionMojo mojo = new CreateVersionMojo();
        String pattern = CreateVersionMojo.BUILD_PATTERN_DEFAULT;
        assertEquals(1002003, mojo.parseBuildNumber("1.2.3", pattern));
        assertEquals(1020030, mojo.parseBuildNumber("1.20.30", pattern));
        assertEquals(1200300, mojo.parseBuildNumber("1.200.300", pattern));
        assertEquals(1002000, mojo.parseBuildNumber("1.2", pattern));
        assertEquals(1020000, mojo.parseBuildNumber("1.20", pattern));
        assertEquals(1002003, mojo.parseBuildNumber("1.2.3-beta1", pattern));
    }

    @Test
    public void testAlternatePattern() {
        CreateVersionMojo mojo = new CreateVersionMojo();
        String pattern = "%1d%02d";
        assertEquals(102, mojo.parseBuildNumber("1.2.3", pattern));
        assertEquals(120, mojo.parseBuildNumber("1.20.30", pattern));
        assertEquals(102, mojo.parseBuildNumber("1.2", pattern));
        assertEquals(120, mojo.parseBuildNumber("1.20", pattern));
    }
}
