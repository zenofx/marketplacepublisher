# Atlassian Marketplace Publisher
Maven plugin for creating new app versions in the Atlassian Marketplace.
By default integrated into maven "deploy" lifecycle.

Copyright (C) 2019 zenofx.com

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Integration
Insert into your pom.xml:
```xml
    <plugins>
      ...
      <!-- Marketplace deployer -->
      <plugin>
        <groupId>com.zenofx.maven</groupId>
        <artifactId>marketplace-maven-plugin</artifactId>
        <configuration>
          <username>user@example.com</username>
          <password>{token encrypted by mvn -ep}</password>
          <licenseType>GPL</licenseType>
          <paymentModel>FREE</paymentModel>
          <releaseSummary>Summary</releaseSummary>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>createVersion</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!-- Switch off default deployer -->
      <plugin>
        <artifactId>maven-deploy-plugin</artifactId>
        <configuration>
          <skip>true</skip>
        </configuration>
      </plugin>
    </plugins>
```
You're done!

## Usage
```
atlas-mvn clean deploy
```
## Supported configuration properties
`username`
  Username for Atlassian Marketplace.
  
`password`
  Token for Atlassian Marketplace (passwords are not supported any more).

`buildNumber`
  Build number, if not set computed from semantic version.

`buildPattern` (Default: %1d%03d%03d)
  Pattern for computing build number from semantic version. 
  Default value according to Atlassian versioning proposals.

`licenseType`
  License type (COMMERCIAL, COMMERCIAL_FREE, BSD, ASL, GPL, LGPL, EPL or ATLASSIAN_CLOSED_SOURCE).

`paymentModel`
  Payment Model (FREE, PAID_VIA_VENDOR or PAID_VIA_ATLASSIAN).

`releaseNotes`
  Release notes.

`releaseSummary`
  Release summary.

`status` (Default: PRIVATE)
  Status of version after upload (PRIVATE, PUBLIC or SUBMITTED). 
  This is only for completeness, please always use PRIVATE and submit manually.

`supported` (Default: true)
  Is this version supported?

`beta` (Default: false)
  Is this a beta version?

`type` (Default: JAR)
  Type of artifact (JAR or OBR).
  
